<?php

/**
 * @file
 * Views integration for the form api revisions.
 */

/**
 * Implements hook_views_data().
 */
function form_api_revision_views_data() {
  $data = array();

  $data['form_api_revision_revisions'] = array(
    'table' => array(
      'base' => array(
        'field' => 'frid',
        'title' => t('Form Api Revision'),
        'help' => t('Provide access a data of custom table.'),
      ),
      'group' => 'Form Api Revision',
    ),
    'frid' => array(
      'title' => t('Form Api Revision Id'),
      'help' => t('The id of Form Api Revision'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    'uid' => array(
      'title' => t('Uid'),
      'help' => t('Target User id of created the revision'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'relationship' => array(
        'base' => 'users',
        'field' => 'uid',
        'handler' => 'views_handler_relationship',
        'label' => t('Target user'),
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_uid',
        'numeric' => TRUE,
        'validate type' => 'uid',
      ),
    ),
    'fr_form_id' => array(
      'title' => t('Drupal Form Id'),
      'help' => t('String representing the name of the form itself.'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),
    'form' => array(
      'title' => t('Form'),
      'help' => t('Provide serialized data of the nested array of form elements that comprise the form.'),
      'field' => array(
        'handler' => 'views_handler_field_serialized',
      ),
    ),
    'form_values' => array(
      'title' => t('Form values'),
      'help' => t('Provide serialized data of a keyed array containing the current state of the form.'),
      'field' => array(
        'handler' => 'views_handler_field_serialized',
      ),
    ),
    'created' => array(
      'title' => t('Created'),
      'help' => t('The timestamp that the revision was created.'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
    ),
  );

  $data['form_api_revision_revisions']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );

  return $data;
}
