<?php
/**
 * @file
 * Functions related the Form Api Revisions.
 */

/**
 * Get all form ids from database.
 *
 * @return array|mixed
 *   Return array with results from database if founded.
 */
function _form_api_revision_get_all_form_ids() {
  // Get first of static cache.
  $results = &drupal_static(__FUNCTION__);

  if (empty($results)) {
    $results = db_select('form_api_revision_revisions', 'far')
                  ->fields('far', array('fr_form_id'))
                  ->execute()
                  ->fetchAllKeyed(0, 0);
  }
  return $results;
}

/**
 * Get all revisions of form id.
 *
 * @param string $form_id
 *   If of form.
 *
 * @return array;
 *   Return array of revisions from database.
 */
function _form_api_revision_get_revisions_by_form_id($form_id) {

  // Get first of static cache.
  $results = &drupal_static(__FUNCTION__);

  if (!empty($results)) {
    return $results;
  }

  module_load_include('inc', 'form_api_revision', 'includes/form_api_revision.helper');

  $results = db_select('form_api_revision_revisions', 'far')
                ->fields('far')
                ->condition('fr_form_id', $form_id, '=')
                ->orderBy('created', 'DESC')
                ->execute()
                ->fetchAll(PDO::FETCH_ASSOC);

  if (!empty($results)) {
    $uids = _form_api_revision_array_column($results, 'uid');
    $users = user_load_multiple(array_unique($uids));
    foreach ($results as $key => $result) {
      $results[$key]['user'] = isset($users[$result['uid']]) ? $users[$result['uid']] : _form_api_revision_drupal_anonymous_user();
    }
  }
  return $results;
}

/**
 * Get Revision by id.
 *
 * @param int $frid
 *    Id of revision.
 *
 * @return object
 *   Return data from database as object.
 */
function _form_api_revision_get_revision_by_id($frid) {

  return db_select('form_api_revision_revisions', 'far')
              ->fields('far')
              ->condition('frid', $frid, '=')
              ->orderBy('created', 'DESC')
              ->range(0, 1)
              ->execute()
              ->fetchObject();
}
