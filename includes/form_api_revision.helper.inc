<?php
/**
 * @file
 * Functions related the Form Api Revisions.
 */

/**
 * Generates a default anonymous $user object.
 *
 * @return object
 *   The user object.
 */
function _form_api_revision_drupal_anonymous_user() {
  $anonymous_user = drupal_anonymous_user();
  $anonymous_user->name = t('Anonymous user');
  return $anonymous_user;
}

/**
 * Return the values from a single column in the input array.
 *
 * @see http://php.net/manual/en/function.array-column.php
 */
function _form_api_revision_array_column(array $records, $field) {

  if (function_exists('array_column')) {
    return array_column($records, $field);
  }

  // Fallback for php < 5.5.
  return array_map(function($element) use($field) {
    return $element[$field];
  }, $records);
}
