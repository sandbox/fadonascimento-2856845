INTRODUCTION
------------

This is a developer module.

This module allows you to create revisions to your custom forms, providing information for the user that changed and when the change was made, thus providing a power of control over what, when, and who changed the data in the custom form.

For example, enable the module "Form Api Revisions Example" and access the
url: "admin/config/development/form_api_revision_example", fill the fields e save it, after saving a button "Show/Hide Revisions" is visible as well, click it and the revisions will be displayed.

REQUIREMENTS
------------

This module requires the following modules:

 *Chaos tool suite (ctools) (https://www.drupal.org/project/ctools)

INSTALLATION
------------

 *Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

  It's so easy to use, just add this "form_api_revision_system_settings_form" function instead of the "system_settings_form" function in your custom forms. For the forms with custom submits just add the follow code in your form:
  "$form['#submit'][] = 'form_api_revision_save_current_revision';"
  Please check the module "form_api_revision_example" as example the implementation.

SUPPORT
-------

  Please use the issue queue for filing bugs with this module at
  http://drupal.org/project/issues/form_api_revision

MAINTAINERS
-----------

  Current maintainers:
  *Fabricio Nascimento (fadonascimento)- https://www.drupal.org/u/fadonascimento
